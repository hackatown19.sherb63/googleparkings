﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParkingAssistant.Common;
using ParkingAssistant.GoogleMaps;

namespace ParkingAssistant.Models
{
    public class MapsApiModel
    {
        HttpClient Client => new HttpClient();

        public MapsApiModel()
        {
            Client.BaseAddress = new Uri(MapsApiSettings.Url);
        }

        public string GetCurrentLocation()
        {
            var responseString = Client.PostAsync(MapsApiSettings.GeolocationUrl, null)
                                .Result.Content.ReadAsStringAsync();
            var jsonObject = JObject.Parse(responseString.Result.ToString());

            return jsonObject.GetValue("location").ToString();
        }

        public string GetClosestLocation(string[] destinationCoordinates)
        {
            var paramSeparater = "&";

            var closestLocation = "";
            var origin = JObject.Parse(GetCurrentLocation());
            var originLatitude = (string) origin.GetValue("lat");
            var originLongitude = (string) origin.GetValue("lng");

            var originFormated = $"origins={originLatitude},{originLongitude}";

            var destinationsFormatted = "destinations=";
            foreach (var coordinate in destinationCoordinates)
            {
                destinationsFormatted += coordinate +"|";
            }

            var parameters = $"{originFormated}{paramSeparater}{destinationsFormatted}";
            var request = MapsApiSettings.DistanceMatrixUrl + parameters + paramSeparater + $"key={MapsApiSettings.Token}";

            var responseString = Client.PostAsync(request, null)
                .Result.Content.ReadAsStringAsync().Result.ToString();

            var routesStats = (JArray)JObject.Parse(responseString)["rows"];
            var routesStatsSpecs = (JArray)routesStats[0]["elements"];

            var minTimeElement = routesStatsSpecs[0];
            var minDuration = ((JObject)minTimeElement).GetValue("duration");
            var minTime = (int)((JObject)minDuration).GetValue("value");
            var minDurationText = (string)((JObject)minDuration).GetValue("text");
            var minDistance = ((JObject)minTimeElement).GetValue("distance");
            var minDistanceText = (string)((JObject)minDistance).GetValue("text");

            int index = 0;
            int minIndex = 0;

            foreach (var element in routesStatsSpecs)
            {
                var temp = ((JObject) element).GetValue("duration");
                if ((int) ((JObject) temp).GetValue("value") < minTime)
                {
                    minDuration = temp;
                    minTime = (int)((JObject)minDuration).GetValue("value");
                    minDurationText = (string)((JObject)minDuration).GetValue("text");
                    minDistance = ((JObject)element).GetValue("distance");
                    minDistanceText = (string)((JObject)minDistance).GetValue("text");
                    minIndex = index;
                }

                index++;
            }

            var destination = destinationCoordinates[minIndex];
            closestLocation =
                "{"+
                    "\"metrics\": {" + 
                        "\"distance\":" + "\"" + minDistanceText + "\", "+
                        "\"duration\":" + "\"" + minDurationText + "\"" +
                    "}," +
                    "\"location\": { " +
                        "\"streetAddress\": \"" + JObject.Parse(responseString)["destination_addresses"][minIndex] + "\", " +
                        "\"lat\": \"" + destination.Split(",")[0] + "\", " +
                        "\"lng\": \"" + destination.Split(",")[1] + "\"" +
                    "}" +
                "}";

            ShowOnMaps(destination);

            return closestLocation;
        }

        public void ShowOnMaps(string coordinates)
        {
            
            var processString = @"http:\\www.google.com\maps\place\" + coordinates;
            Process.Start(processString);
        }
    }
}
