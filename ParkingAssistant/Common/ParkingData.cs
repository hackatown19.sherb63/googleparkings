﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ParkingAssistant.Common
{
    public class ParkingData
    {
        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public ObjectId ID { get; set; }

        public override string ToString()
        {
            return JObject.FromObject(this).ToString();
        }
    }
}
