﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingAssistant.Models;

namespace ParkingAssistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapsApiController : ControllerBase
    {
        private MapsApiModel MapsModel => new MapsApiModel();
        private MongoModel DbModel => new MongoModel();

        // GET api/mapsapi
        [HttpGet]
        public ActionResult<string> Get()
        {
            return MapsModel.GetCurrentLocation();
        }

        // GET api/mapsapi/parking
        [HttpGet("parking")]
        public ActionResult<string> Get(int id)
        {
            DbModel.GetAvailableParkings();
            string [] sauce = { "44.50715,-73.6098432", "46.50715,-73.6098432", "45.60715,-73.6098432" };
            return MapsModel.GetClosestLocation(sauce);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
