﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingAssistant.GoogleMaps
{
    public static class MapsApiSettings
    {
        public static string Token => "AIzaSyB60a6v-62SSpAgG4Pc_SrfC0UFZ0aK-DI";

        public static string Url => "https://maps.googleapis.com/maps/api/";

        public static string GeolocationUrl => $"https://www.googleapis.com/geolocation/v1/geolocate?key={Token}";

        public static string DistanceMatrixUrl => "https://maps.googleapis.com/maps/api/distancematrix/json?";
    }
}
